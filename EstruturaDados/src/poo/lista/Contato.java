
package poo.lista;

import java.util.Date;

public class Contato {
String nome;
String telefone;
Integer anoNascimento;
Date dataNascimento;
public String getNome() {
	return nome;
}
public void setNome(String nome) {
	this.nome = nome;
}
public String getTelefone() {
	return telefone;
}
public void setTelefone(String telefone) {
	this.telefone = telefone;
}
public Integer getAnoNascimento() {
	return anoNascimento;
}
public void setAnoNascimento(Integer anoNascimento) {
	this.anoNascimento = anoNascimento;
}
public Date getDataNascimento() {
	return dataNascimento;
}
public void setDataNascimento(Date dataNascimento) {
	this.dataNascimento = dataNascimento;
}

}

